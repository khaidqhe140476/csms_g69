﻿using Microsoft.Extensions.DependencyInjection;
using CSMSBE.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSMSBE.Services.Interfaces;
using CSMSBE.Infrastructure.Base.Interfaces;
using CSMSBE.Infrastructure.Base.Implements;
using CSMSBE.Services.Implements;
using CSMSBE.Infrastructure.Implements;

namespace CSMSBE.Api
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddScoped<ISomeTableService, SomeTableService>();
            services.AddScoped<ISomeTableRepository, SomeTableRepository>();
            /*services.AddScoped<ISomeTableRepository, SomeTableRepository>();*/
            // Add other services here

            return services;
        }
    }
}
