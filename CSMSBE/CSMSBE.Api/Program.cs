using Microsoft.EntityFrameworkCore;
using System;
using CSMSBE.Infrastructure;
using CSMSBE.Infrastructure.Contexts;
using AutoMapper;
using CSMSBE.Services.Configurations;
using CSMSBE.Api;
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(MappingProfile));
builder.Services.AddApplicationServices();
var configuration = builder.Configuration;
builder.Services.AddLogging();
builder.Services.AddDbContext<CSMSDbContext>(options =>
    options.UseNpgsql(configuration.GetConnectionString("MyWebApiConection")));
var app = builder.Build();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
};
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
