﻿using CSMSBE.Core;
using CSMSBE.Core.Helper;
using CSMSBE.Core.Interfaces;
using CSMSBE.Core.Resource;
using CSMSBE.Services.DTO.FilterRequest;
using CSMSBE.Services.DTO.SomeTableDTO;
using CSMSBE.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CSMSBE.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SomeTableController : ControllerBase
    {
        private readonly ISomeTableService _someTableService;
        private readonly ILogger<SomeTableController> _logger;
        public SomeTableController(ISomeTableService someTableService, ILogger<SomeTableController> logger) { 
            _someTableService = someTableService;
            _logger = logger;
        }
        // GET: api/<SomeTableController>
        [HttpGet("GetAllData")]
        public ActionResult<ResponseItem<IEnumerable<SomeTableDTO>>> GetAllData()
        {
            try
            {
                var results = _someTableService.GetAll();
                return Ok(new ResponseData() { Content = results });
            }catch(Exception ex)
            {
                return BadRequest(new ResponseErrorData
                {
                    ErrorType = $"{ErrorTypeConstant.DataNotValid} {ex}",
                    ErrorMessage = StringMessage.ErrorMessages.ErrorProcess,
                });
            }
        }

        public void ValidateInputFilter(SomeTableFilterRequest filter) 
        {
            /*if (true)
            {
                throw new ArgumentNullException(nameof(filter));
            }*/
        }
        [HttpGet("GetFilteredData")]
        public async Task<ActionResult<ResponseItem<IPagedList<SomeTableDTO>>>> GetFilteredData([FromQuery] SomeTableFilterRequest filter)
        {
            try
            {
                ValidateInputFilter(filter);
                var results = await _someTableService.GetFilteredData(filter);
                var x = new ResponseData() { Content = results };
                return Ok(new ResponseData() { Content = results });
            }
            catch (Exception ex)
            {
                return BadRequest(new ResponseErrorData
                {
                    ErrorType = $"{ErrorTypeConstant.DataNotValid} {ex}",
                    ErrorMessage = StringMessage.ErrorMessages.ErrorProcess,
                });
            }
        }

        // GET api/<SomeTableController>/5
        [HttpGet("GetSomeTableById/{id}")]
        public ActionResult<ResponseItem<SomeTableDTO>> Get(int id)
        {
            try
            {
                var result = _someTableService.Get(id);
                return Ok(new ResponseData() { Content = result });
            }
            catch (Exception ex)
            {
                return BadRequest(new ResponseErrorData
                {
                    ErrorType = $"{ErrorTypeConstant.DataNotValid} {ex}",
                    ErrorMessage = StringMessage.ErrorMessages.ErrorProcess,
                });
            }         
        }

        // POST api/<SomeTableController>
        [HttpPost("CreateSomeTable")]
        public ActionResult<ResponseItem<SomeTableDTO>> CreateSomeTable(CreateSomeTableDTO dto)
        {
            try
            {
                var createResult = _someTableService.Create(dto);
                return Ok(new ResponseData() { Content = createResult });
            }
            catch (Exception ex)
            {
                return BadRequest(new ResponseErrorData
                {
                    ErrorType = $"{ErrorTypeConstant.DataNotValid} {ex}",
                    ErrorMessage = StringMessage.ErrorMessages.ErrorProcess,
                });
            }      
            
        }

        // PUT api/<SomeTableController>/5
        [HttpPut("UpdateSomeTable")]
        public ActionResult<ResponseItem<SomeTableDTO>> Put([FromBody] UpdateSomeTableDTO updateData)
        {
            try
            {
                var updateResult = _someTableService.Update(updateData);
                return Ok(new ResponseData() { Content = updateResult });
            }
            catch (Exception ex)
            {
                return BadRequest(new ResponseErrorData
                {
                    ErrorType = $"{ErrorTypeConstant.DataNotValid} {ex}",
                    ErrorMessage = StringMessage.ErrorMessages.ErrorProcess,
                });
            }         
        }

        // DELETE api/<SomeTableController>/5
        [HttpDelete("RemoveSomeTableById/{id}")]
        public ActionResult<ResponseItem<bool>> Delete(int id)
        {
            try
            {
                var result = _someTableService.Remove(id);
                    return Ok(new ResponseData() { Content = result });
            }
            catch (Exception ex)
            {
                return BadRequest(new ResponseErrorData
                {
                    ErrorType = $"{ErrorTypeConstant.DataNotValid} {ex}",
                    ErrorMessage = StringMessage.ErrorMessages.ErrorProcess,
                });
            }
           
        }
    }
}
