using CSMSBE.Core;
using Microsoft.AspNetCore.Mvc;

namespace CSMSBE.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
  /*      private readonly CSMSDbContext _context;*/
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public ActionResult Get()
        {
            try
            {
                string x = "aefwaefawf";
                int p = Int16.Parse(x);
                return Ok(new ResponseData()
                {
                    Content = Enumerable.Range(1, 5).Select(index => new WeatherForecast
                    {
                        Date = DateTime.Now.AddDays(index),
                        TemperatureC = Random.Shared.Next(-20, 55),
                        Summary = Summaries[Random.Shared.Next(Summaries.Length)]
                    })
            .ToArray(),
                    Err = "",
                });
            }
            catch  (Exception ex)
            {
                return BadRequest(new ResponseData()
                {
                    Content = null,
                    Err = ex.Message,
                });
            }
            
        }
    }
}
