﻿using CSMSBE.Infrastructure.Base.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CSMSBE.Core.Entities.BaseEntities;

namespace CSMSBE.Infrastructure.Base.Implements
{
    public class Repository<TEntity, T> : IRepository<TEntity, T> where TEntity : BaseFullAuditedEntity<T>
    {
        private readonly DbContext _context;
        private DbSet<TEntity> entities;

        public Repository(DbContext context)
        {
            _context = context;
            entities = context.Set<TEntity>();
        }

        public bool Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(T id)
        {
            throw new NotImplementedException();
        }

        public bool DeleteMulti(List<TEntity> entity)
        {
            throw new NotImplementedException();
        }

        public TEntity Find(Expression<Func<TEntity, bool>> expression, params Expression<Func<TEntity, object>>[] includes)
        {
            throw new NotImplementedException();
        }

        public List<TEntity> FindAll(Expression<Func<TEntity, bool>> expression, params Expression<Func<TEntity, object>>[] includes)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public IQueryable<TEntity> GetAllInclude(params Expression<Func<TEntity, object>>[] includes)
        {
            throw new NotImplementedException();
        }

        public Task<TEntity> GetAsyncById(T id)
        {
            throw new NotImplementedException();
        }

        public TEntity GetById(T id)
        {
            throw new NotImplementedException();
        }

        public TEntity GetByIdInclude(T id, params Expression<Func<TEntity, object>>[] includes)
        {
            throw new NotImplementedException();
        }

        public IList<TEntity> GetListAllAsync()
        {
            throw new NotImplementedException();
        }

        public TEntity Insert(TEntity entity)
        {
            var result = _context.Add(entity);
            _context.SaveChanges();
            return result.Entity;
        }

        public async Task<TEntity> InsertAsync(TEntity entity)
        {
            var result = await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public List<TEntity> InsertMulti(List<TEntity> entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public TEntity Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public List<TEntity> UpdateMulti(List<TEntity> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
