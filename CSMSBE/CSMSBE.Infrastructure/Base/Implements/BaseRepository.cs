﻿using CSMSBE.Infrastructure.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CSMSBE.Infrastructure.Base.Implements
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        public bool Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(dynamic id)
        {
            throw new NotImplementedException();
        }

        public bool DeleteMulti(List<T> entity)
        {
            throw new NotImplementedException();
        }

        public T Find(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes)
        {
            throw new NotImplementedException();
        }

        public List<T> FindAll(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<T> GetAsyncById(long id)
        {
            throw new NotImplementedException();
        }

        public T GetById(long id)
        {
            throw new NotImplementedException();
        }

        public T GetById(string key)
        {
            throw new NotImplementedException();
        }

        public IList<T> GetListAllAsync()
        {
            throw new NotImplementedException();
        }

        public T Insert(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<T> InsertAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public List<T> InsertMulti(List<T> entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Query(Expression<Func<T, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public T Update(T entity)
        {
            throw new NotImplementedException();
        }

        public List<T> UpdateMulti(List<T> listItem)
        {
            throw new NotImplementedException();
        }
    }
}
