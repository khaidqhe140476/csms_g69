﻿using CSMSBE.Core.Entities;
using CSMSBE.Core.FilterRequest;
using CSMSBE.Infrastructure.Contexts;
using CSMSBE.Infrastructure.Interfaces;
using ImageMagick;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CSMSBE.Infrastructure.Implements
{
    public class SomeTableRepository : ISomeTableRepository
    {
        private readonly CSMSDbContext _context;
        private readonly ILogger<SomeTableRepository> _logger;
        public SomeTableRepository(CSMSDbContext context, ILogger<SomeTableRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public SomeTable Create(SomeTable entity)
        {
            try
            {
                entity.CreatedDate = DateTime.Now;
                entity.IsDelete = false;
                var result = _context.SomeTables.Add(entity);
                _context.SaveChanges();
                return result.Entity;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }

        public SomeTable Get(int id)
        {
            try
            {
                var result = _context.SomeTables.FirstOrDefault(x => x.Id == id);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }

        public IQueryable<SomeTable> GetAll()
        {
            try
            {
                var result = _context.SomeTables.AsQueryable();
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }

        public IQueryable<SomeTable> GetAll(SomeTableFilterRequest filter)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            try
            {
                var entity = _context.SomeTables.FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    return false;
                }
                entity.IsDelete = true;
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }

        public SomeTable Update(SomeTable updateData)
        {
            try
            {
                var entity = _context.SomeTables.Find(updateData.Id);
                if (entity != null)
                {
                    _context.Entry(entity).CurrentValues.SetValues(updateData);
                    entity.ModifiedDate = DateTime.Now;
                    _context.SaveChanges();
                    return _context.SomeTables.Find(updateData.Id);
                }
                else
                {
                    throw new Exception("Không tìm thấy bản ghi để cập nhật");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }
    }
}
