﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using CSMSBE.Core.Entities;

namespace CSMSBE.Infrastructure.Contexts
{
    public partial class CSMSDbContext : DbContext
    {
        public CSMSDbContext()
        {
        }

        public CSMSDbContext(DbContextOptions<CSMSDbContext> options)
            : base(options)
        {
        }
        public virtual DbSet<SomeTable> SomeTables { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=localhost;Database=postgres;Username=postgres;Password=123456");
            }
        }      
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
