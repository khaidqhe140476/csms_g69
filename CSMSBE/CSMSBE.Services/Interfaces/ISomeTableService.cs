﻿using CSMSBE.Core.Entities;
using CSMSBE.Core.Interfaces;
using CSMSBE.Services.DTO.FilterRequest;
using CSMSBE.Services.DTO.SomeTableDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSMSBE.Services.Interfaces
{
    public interface ISomeTableService
    {
        SomeTableDTO Create(CreateSomeTableDTO table);
        IQueryable<SomeTableDTO> GetAll();
        SomeTableDTO Get(int id);
        Task<IPagedList<SomeTableDTO>> GetFilteredData(SomeTableFilterRequest filter);
        SomeTableDTO Update(UpdateSomeTableDTO table);
        bool Remove(int id);
    }
}
