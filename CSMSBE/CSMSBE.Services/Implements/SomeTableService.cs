﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CSMSBE.Core.Entities;
using CSMSBE.Core.Extensions;
using CSMSBE.Core.Helper;
using CSMSBE.Core.Implements;
using CSMSBE.Core.Interfaces;
using CSMSBE.Infrastructure.Interfaces;
using CSMSBE.Services.BaseServices;
using CSMSBE.Services.DTO.FilterRequest;
using CSMSBE.Services.DTO.SomeTableDTO;
using CSMSBE.Services.Interfaces;
using ImageMagick;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace CSMSBE.Services.Implements
{
    public class SomeTableService : ISomeTableService
    {
        private readonly ISomeTableRepository _someTableRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<SomeTableService> _logger;
        public SomeTableService(ISomeTableRepository someTableRepository, IMapper mapper, ILogger<SomeTableService> logger)
        {
            _someTableRepository = someTableRepository;
            _mapper = mapper;
            _logger = logger;
        }
        public SomeTableDTO Create(CreateSomeTableDTO table)
        {
            try
            {
                var result = _someTableRepository.Create(_mapper.Map<SomeTable>(table));
                return _mapper.Map<SomeTableDTO>(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }

        public SomeTableDTO Get(int id)
        {
            try
            {
                var result = _mapper.Map<SomeTableDTO>(_someTableRepository.Get(id));
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }

        public IQueryable<SomeTableDTO> GetAll()
        {
            try
            {
                var result = _someTableRepository.GetAll().ProjectTo<SomeTableDTO>(_mapper.ConfigurationProvider);
                return result;               
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }

        public async Task<IPagedList<SomeTableDTO>> GetFilteredData(SomeTableFilterRequest filter)
        {
            try
            {
                if (filter.PageIndex == 0) filter.PageIndex = Constant.DefaultPageIndex;
                if (filter.PageSize == 0) filter.PageSize = Constant.DefaultPageSize;
                /*var result = _someTableRepository.GetAll().ProjectTo<SomeTableDTO>(_mapper.ConfigurationProvider);*/
                var query = await QueryFilter(filter);
                var response = query.ToPagedList(filter.PageIndex, filter.PageSize);
                var result = response.Map<SomeTableDTO, SomeTable>(_mapper);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }
        public async Task<IQueryable<SomeTable>> QueryFilter(SomeTableFilterRequest filter)
        {
            if (!filter.IsDelete.HasValue)
            {
                filter.IsDelete = false;
            }
            List<long> projectIds = new List<long>();
            var query = _someTableRepository.GetAll()
                .WhereIf(filter.Id.HasValue, x => x.Id == filter.Id)
                .WhereIf(!string.IsNullOrEmpty(filter.NormalText), x => x.NormalText == filter.NormalText)
                .WhereIf(!string.IsNullOrEmpty(filter.PhoneNumber), x => x.PhoneNumber == filter.PhoneNumber)
                .WhereIf(!string.IsNullOrEmpty(filter.Email), x => x.Email == filter.Email)
                .WhereIf(filter.StartDate.HasValue, x => x.StartDate == filter.StartDate)
                .WhereIf(filter.EndDate.HasValue, x => x.EndDate == filter.EndDate)
                .WhereIf(filter.Status.HasValue, x => x.Status == filter.Status)
                .WhereIf(!string.IsNullOrEmpty(filter.Type), x => x.Type == filter.Type)
                .Where(x => x.IsDelete == filter.IsDelete);


            var SortBy = filter.Sorting;

            if (!string.IsNullOrEmpty(SortBy))
            {
                var SortOrder = SortBy.Substring(SortBy.IndexOf(" "));
                SortBy = SortBy.Substring(0, SortBy.IndexOf(" ")).Trim();
                switch (SortBy)
                {
                    case "Id":
                        SortBy = "Id " + SortOrder;
                        break;
                    case "NormalText":
                        SortBy = "NormalText " + SortOrder;
                        break;
                    case "Email":
                        SortBy = "Email " + SortOrder;
                        break;
                    case "PhoneNumber":
                        SortBy = "PhoneNumber " + SortOrder;
                        break;
                    case "StartDate":
                        SortBy = "StartDate " + SortOrder;
                        break;
                    case "EndDate":
                        SortBy = "EndDate " + SortOrder;
                        break;
                    case "Status":
                        SortBy = "Status " + SortOrder;
                        break;
                    case "Type":
                        SortBy = "Type " + SortOrder;
                        break;
                    default:
                        SortBy = filter.Sorting;
                        break;
                }
            }
            /*else SortBy = "ModifiedDate Desc";*/

            if (!string.IsNullOrEmpty(SortBy))
                query = query.Sort(SortBy);
            return query;
            //return await Task.FromResult(query);
        }
        public bool Remove(int id)
        {
            try
            {
                var result = _someTableRepository.Remove(id);
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }

        public SomeTableDTO Update(UpdateSomeTableDTO dto)
        {
            try
            {
                var result = _someTableRepository.Update(_mapper.Map<SomeTable>(dto));
                return _mapper.Map<SomeTableDTO>(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }
    }
}
