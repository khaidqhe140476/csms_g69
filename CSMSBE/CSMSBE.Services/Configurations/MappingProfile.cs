﻿using AutoMapper;
using CSMSBE.Core.Entities;
using CSMSBE.Services.DTO.SomeTableDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSMSBE.Services.Configurations
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Ví dụ ánh xạ từ Entity sang DTO
            CreateMap<SomeTable, SomeTableDTO>().ReverseMap();
            CreateMap<UpdateSomeTableDTO, SomeTable>();
            CreateMap<CreateSomeTableDTO, SomeTable>();
        }
    }
}
