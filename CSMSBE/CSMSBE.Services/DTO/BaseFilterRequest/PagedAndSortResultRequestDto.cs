﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CSMSBE.Services.DTO.BaseFilterRequest
{
    public class PagedAndSortResultRequestDto : PagedResultRequestDto, ISortResultRequestDto
    {
        [DefaultValue("CreatedDate desc")]
        public string Sorting { get; set; }
        public bool? IsDelete { get; set; }
    }
}
