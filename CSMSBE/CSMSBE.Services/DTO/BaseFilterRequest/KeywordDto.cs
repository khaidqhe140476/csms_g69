﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSMSBE.Services.DTO.BaseFilterRequest
{
    public class KeywordDto : IKeywordDto
    {
        public string Keyword { get ; set ; }
    }
}
