﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSMSBE.Services.DTO.BaseFilterRequest
{
    public interface IPagedAndSortResultRequestDto: IPagedResultRequestDto, ISortResultRequestDto
    {
    }
}
