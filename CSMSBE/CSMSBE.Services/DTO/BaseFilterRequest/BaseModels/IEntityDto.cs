﻿namespace CSMSBE.Services.DTO.BaseFilterRequest.BaseModels
{
    public interface IEntityDto<T>
    {
        T Id { get; set; }
    }
}
