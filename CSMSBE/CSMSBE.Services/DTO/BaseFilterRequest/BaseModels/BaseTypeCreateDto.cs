﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSMSBE.Services.DTO.BaseFilterRequest.BaseModels
{
    public class BaseTypeCreateDto
    {
        public string Name { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
    }
}
