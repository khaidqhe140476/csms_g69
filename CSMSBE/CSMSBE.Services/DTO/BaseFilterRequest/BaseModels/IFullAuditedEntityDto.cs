﻿namespace CSMSBE.Services.DTO.BaseFilterRequest.BaseModels
{
    public interface IFullAuditedEntityDto<T> : IEntityDto<T>, IAuditedEntityDto
    {
    }
}
