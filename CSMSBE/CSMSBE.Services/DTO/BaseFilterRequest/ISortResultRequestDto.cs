﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSMSBE.Services.DTO.BaseFilterRequest
{
    public interface ISortResultRequestDto
    {
        string Sorting { get; set; }
    }
}
