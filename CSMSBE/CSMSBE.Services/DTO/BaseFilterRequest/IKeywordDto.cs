﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSMSBE.Services.DTO.BaseFilterRequest
{
    public interface IKeywordDto
    {
        string Keyword { get; set; }
    }
}
