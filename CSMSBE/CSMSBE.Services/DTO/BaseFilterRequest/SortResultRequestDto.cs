﻿namespace CSMSBE.Services.DTO.BaseFilterRequest
{
    public class SortResultRequestDto : ISortResultRequestDto
    {
        public string Sorting { get; set; }
    }
}
