﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace CSMSBE.Core.Entities
{
    [Table("SomeTable")]
    public partial class SomeTable
    {
        [Key]
        [Column("id")]
        public int? Id { get; set; }
        [Column("normalText")]
        public string? NormalText { get; set; }
        [Column("phoneNumber")]
        public string? PhoneNumber { get; set; }
        [Column("email")]
        public string? Email { get; set; }
        [Column("startDate", TypeName = "timestamp without time zone")]
        public DateTime? StartDate { get; set; }
        [Column("endDate", TypeName = "timestamp without time zone")]
        public DateTime? EndDate { get; set; }
        [Column("status")]
        public bool? Status { get; set; }
        [Column("type")]
        public string? Type { get; set; }
        [Column("created_by")]
        public string? CreatedBy { get; set; }

        [Column("created_date", TypeName = "timestamp without time zone")]
        public DateTime? CreatedDate { get; set; }

        [Column("modified_by")]
        public string? ModifiedBy { get; set; }

        [Column("modified_date", TypeName = "timestamp without time zone")]
        public DateTime? ModifiedDate { get; set; }
        [Column("is_delete")]
        public bool? IsDelete { get; set; }
    }
}
