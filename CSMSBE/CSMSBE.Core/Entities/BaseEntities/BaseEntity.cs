﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSMSBE.Core.Entities.BaseEntities
{
    public class BaseEntity<T>
    {
        [Key]
        [Column("id")]
        public T Id { get; set; }
    }
}
