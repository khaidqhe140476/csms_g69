import ApiConfig from "./api-config";
import {
  setConfiguration,
  ApiServerKey,
  TokenKey, TokenPrefix, getCookies
} from "@utils/configuration";
import AxiosInterceptor from "./api-config-interceptors";

setConfiguration(
  ApiServerKey.APP_API_ROOT,
  ApiConfig.api
);

export default class Service {
  constructor() {}

  /**
   * Get Http Request
   * @param {any} action
   */
  get(action: string, params: any) {
    return new Promise((resolve, reject) => {
      AxiosInterceptor.get(params ? action + "?" + params : action, {
        method: "GET",
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          if (
            error.response &&
            error.response.data &&
            error.response.data.error
          ) {
            console.error("REST request error!", error.response.data.error);
            reject(error.response.data.error);
          } else reject(error);
        });
    });
  }

  /**
   * Post Http Request
   * @param {any} action
   * @param {any} params
   */
  postParams(action: string, params: any, body: any) {
    return new Promise((resolve, reject) => {
      AxiosInterceptor.post(params ? action + "?" + params : action, {
        method: "POST",
        data: body,
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          if (
            error.response &&
            error.response.data &&
            error.response.data.error
          ) {
            console.error("REST request error!", error.response.data.error);
            reject(error.response.data.error);
          } else reject(error);
        });
    });
  }

  /**
   * Get Http Request
   * @param {any} action
   */
  getBinary(action: string, params: any) {
    return new Promise((resolve, reject) => {
      AxiosInterceptor.get(params ? action + "?" + params : action, {
        method: "GET",
        responseType: "blob",
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          if (
            error.response &&
            error.response.data &&
            error.response.data.error
          ) {
            console.error("REST request error!", error.response.data.error);
            reject(error.response.data.error);
          } else reject(error);
        });
    });
  }
  /**
   * Get Http Request
   * @param {any} action
   */
  postBinary(action: string, params: any, body: any, callback: Function = () => {}) {
    callback(0);
    let userInfo = getCookies(TokenKey.token);
    let headers = {};

    if (userInfo) {
      headers = {
        Authorization: `${TokenPrefix} ${userInfo}`,
      };
    }
    return new Promise((resolve, reject) => {
      AxiosInterceptor.post(params ? action + "?" + params : action, {
        data: body,
        method: "POST",
        responseType: "blob",
        onDownloadProgress: (progressEvent: ProgressEvent) => {
          const { loaded, total } = progressEvent;
          let percentCompleted = Math.floor((loaded * 100) / total);
          callback(percentCompleted);
        },
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          if (
            error.response &&
            error.response.data &&
            error.response.data.error
          ) {
            console.error("REST request error!", error.response.data.error);
            reject(error.response.data.error);
          } else reject(error);
        });
    });
  }

  /**
   * Post Http Request
   * @param {any} action
   * @param {any} params
   */
  post(action: string, params: any) {
    return new Promise((resolve, reject) => {
      AxiosInterceptor.post(action, {
        method: "POST",
        data: params,
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          if (
            error.response &&
            error.response.data &&
            error.response.data.error
          ) {
            console.error("REST request error!", error.response.data.error);
            reject(error.response.data.error);
          } else reject(error);
        });
    });
  }

  /**
   * Put Http Request
   * @param {any} action
   * @param {any} params
   */
  put(action: string, params: any, requestBody: any) {
      return new Promise((resolve, reject) => {
      AxiosInterceptor.put(params ? action + "?" + params : action, {
        method: "PUT",
        data: requestBody,
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          if (
            error.response &&
            error.response.data &&
            error.response.data.error
          ) {
            console.error("REST request error!", error.response.data.error);
            reject(error.response.data.error);
          } else reject(error);
        });
    });
  }
  /**
   *Delete Http Request
   * @param {any} action
   * @param {any} params
   */
  delete(action: string, params: any) {
    return new Promise((resolve, reject) => {
      AxiosInterceptor.delete(params ? action + "?" + params : action, {
        method: "DELETE",
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          if (
            error.response &&
            error.response.data &&
            error.response.data.error
          ) {
            console.error("REST request error!", error.response.data.error);
            reject(error.response.data.error);
          } else reject(error);
        });
    });
  }
}
