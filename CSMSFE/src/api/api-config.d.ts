declare module '@common/api-config' {
    interface ApiEnvironment {
      api: string;
      paht_api: string;
      media_url: string;
      domainAdminSide: string;
      domainUserSide: string;
      domainName: string;
      // workSpace?: string;
      // wmsBaseLink?: string;
    }
  
    interface ApiEnvironmentConfig {
      development: ApiEnvironment;
      production: ApiEnvironment;
    }
  
    const apiEnvironment: ApiEnvironmentConfig;
  
    const ApiConfig: ApiEnvironment;
  
    export default ApiConfig;
}
  