import axios, { AxiosInstance } from "axios";
import {
  TokenKey,
  TokenPrefix,
  getCookies,
  removeCookies,
  setCookiesUser,
  setConfiguration,
  getConfiguration,
  ApiServerKey,
} from "@utils/configuration";
import {GenerateDeviceId} from  "@/common/tools";
import ApiConfig from "@/api/api-config";

const DomainAdminSide = ApiConfig.domainAdminSide;

setConfiguration(
  ApiServerKey.APP_API_ROOT,
  ApiConfig.api
);

class AxiosInterceptor {
  private axiosInstance: AxiosInstance;
  private apiRoot: string;
  private domainAdminSide: string;
  private isHandlerEnabled: boolean = true;

  constructor() {
    this.apiRoot = getConfiguration(ApiServerKey.APP_API_ROOT);
    this.domainAdminSide = DomainAdminSide;
    this.isHandlerEnabled = true;
    this.axiosInstance = axios.create({
      baseURL: this.apiRoot,
      responseType: "json",
    });

    this.initializeInterceptors();
  }

  private initializeInterceptors() {
    this.axiosInstance.interceptors.request.use(
      (request) => this.requestHandler(request),
      (error) => this.errorHandler(error)
    );

    this.axiosInstance.interceptors.response.use(
      (response) => this.successHandler(response),
      async (error) => await this.errorResponseHandler(error)
    );
  }

  private requestHandler(request: any) {
    if (this.isHandlerEnabled) {
      request.headers.common["Content-Type"] = "application/json; charset=utf-8";
      request.headers.common["Accept"] =
        "application/json, text/javascript, */*; q=0.01";
      request.headers.common["Access-Control-Allow-Origin"] = "*";

      let userInfo = getCookies(TokenKey.token);
      if (userInfo) {
        request.headers.common["Authorization"] = `${TokenPrefix} ${userInfo}`;
      }
    }

    return request;
  }

  private successHandler(response: any) {
    // Do something with the successful response if needed
    return response;
  }

  private errorHandler(error: any) {
    // Handle request error if needed
    return Promise.reject(error);
  }

  private async errorResponseHandler(error: any) {
    if (error.response && error.response.status === 401) {
      const config = error.config;
      if (!config._retry) {
        config._retry = true;
        try {
          await this.refreshToken();
          config.headers["Authorization"] = "Bearer " + getCookies(TokenKey.token);
          return this.axiosInstance(config);
        } catch (err) {
          return Promise.reject(err);
        }
      }
    }
    return Promise.reject({
      ...(error.response
        ? error.response.data
        : {
            errorType: "Unhandled Exception",
            errorMessage: "Unhandled Exception",
          }),
    });
  }

  public async refreshToken() {
    try {
      let DeviceId = GenerateDeviceId();
      const response = await axios
        .create({
          baseURL: this.apiRoot,
        })
        .post("/api/Account/RefreshToken", {
          refreshToken: getCookies(TokenKey.refreshToken),
          accessToken: getCookies(TokenKey.token),
          returnUrl: this.domainAdminSide,
          DeviceId: DeviceId,
        });
      removeCookies("isShowDialog");
      setCookiesUser(response);
    } catch (error) {
      removeCookies("isShowDialog");
      removeCookies("isLockScreen");
      removeCookies(TokenKey.token);
      removeCookies(TokenKey.refreshToken);
      removeCookies(TokenKey.returnUrl);
      window.location.replace(this.domainAdminSide + "/dang-nhap");
    }
  }

  public getInstance(): AxiosInstance {
    return this.axiosInstance;
  }
}

export default new AxiosInterceptor().getInstance();