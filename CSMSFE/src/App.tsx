import '@styles/styles.css';
import '@styles/custom-styles.css';
import '@fontsource/rubik/300.css';
import '@fontsource/rubik/400.css';
import '@fontsource/rubik/500.css';
import '@fontsource/rubik/700.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import StoreProvider from '@/store';
import AppRouter from '@utils/routes';
import MUITheme from '@utils/theme';
import { Provider as SnackbarProvider } from '@/components/snackbar';
import CustomizationLayout from '@components/layouts/customization';

function App() {
	return (
		<StoreProvider>
			<MUITheme>
				<SnackbarProvider>
					<CustomizationLayout />
					<AppRouter />
				</SnackbarProvider>
			</MUITheme>
		</StoreProvider>
	);
}

export default App;