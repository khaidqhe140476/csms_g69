export interface SomeTable {
    id: number;
    name: string;
    position: string;
    email: string;
    salary: number;
}