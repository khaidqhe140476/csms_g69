interface PagedListContent<T> {
    items: T[];
    pageCount: number;
    totalItemCount: number;
    pageIndex: number;
    pageSize: number;
    hasPreviousPage: boolean;
    hasNextPage: boolean;
}

export interface ApiResponse<T> {
    content: PagedListContent<T>;
    err: any;
}

