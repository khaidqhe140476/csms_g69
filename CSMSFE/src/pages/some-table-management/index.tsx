import { useEffect, useState } from 'react';
import Card from '@mui/material/Card';

import ListSomeTable from './list-some-table/list-some-table';

import employeesData from '@/_mocks/employees';
import PageHeader from '@/components/pageHeader';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import { SomeTable } from '@common/DTO/SomeTableDTO/SomeTableDTO';
import { ApiResponse } from '@/common/DTO/ApiResponse';
import { DEFAULT_PAGE_SIZE, DEFAULT_SORT_EXPRESSION } from '@/common/default-config';
import FormSearchSomeTable from './form-search-some-table/form-search-some-table';

const SomeTableManagement = ({  }: any) => {
    const [data, setData] = useState<SomeTable[]>([]);
    const [totalItemCount, setTotalItemCount] = useState<number>(0);
    const [searchParam, setSearchParam] = useState<any>({});

    useEffect(() => {
        handleFilterSomeTable();
    }, [])

    const handleFilterSomeTable = (
        pageIndex: number = 1, 
        pageSize: number = DEFAULT_PAGE_SIZE,
        sortExpression: string = DEFAULT_SORT_EXPRESSION,
        filterParam: any = searchParam,
    ) => {
        try {
            const res: ApiResponse<SomeTable> = {
                content: {
                    items: employeesData,
                    hasNextPage: true,
                    hasPreviousPage: false,
                    pageSize: 5,
                    pageCount: 5,
                    pageIndex: 3,
                    totalItemCount: employeesData.length
                },
                err: null
            }
            setData(res.content.items);
            setTotalItemCount(res.content.totalItemCount);
        } catch ( err ) {
            console.error("handleFilterSomeTable error", err);
        } finally {

        }
    };
      
	return (
        <>
			<PageHeader title="Quản lý some table">
				<Breadcrumbs
					aria-label="breadcrumb"
					sx={{
						textTransform: 'uppercase',
					}}
				>
					<Typography color="text.tertiary">Components</Typography>
					<Typography color="text.tertiary">Tables</Typography>
                    <Link underline="hover" href="#!">
						Inicio
					</Link>
				</Breadcrumbs>
			</PageHeader>

			<Stack spacing={5}>
                <FormSearchSomeTable />

                <Card component="section">
                    
                    <ListSomeTable
                        rows={data}
                        totalItemCount={totalItemCount}
                        handleFilterAction={handleFilterSomeTable}
                    />
                </Card>
			</Stack>
		</>
	);
}

export default SomeTableManagement;