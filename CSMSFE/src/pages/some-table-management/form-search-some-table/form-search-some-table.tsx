
import { useState } from 'react';
import { useForm } from 'react-hook-form';
// MUI
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import FormInput from '@/components/formInput';

// Icon
import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import SearchOffIcon from '@mui/icons-material/SearchOff';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import FormSearchTogglableWrapper from '@/components/formSearchTogglableWrapper';

// Media query breakpoint
import { useMediaQuery } from "react-responsive";

const FORM_SCHEMA = {
	nombre: {},
	apellidos: {},
	textArea: {},
	select: {},
	password: {},
};

const FormSearchSomeTable = () => {
    //media query
    const isXS = useMediaQuery({ query: "(min-width: 900px)" });
    const isMD = useMediaQuery({ query: "(min-width: 600px)" });

	const methods = useForm({
		mode: 'onChange',
		defaultValues: {
			nombre: '',
			apellidos: '',
			password: '',
		},
	});
    const { control, handleSubmit, formState: { errors }, reset} = methods;

	const onSubmit = (data: any) => console.log(data);

    const [isOpenSearch, setIsOpenSearch] = useState(false);

    const onResetSearch = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault();
        reset();
    }

	return (
        <Card component="section">
            <form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                <FormSearchTogglableWrapper
                    isOpen={isOpenSearch}
                    numOfLines={isXS ? 2 : isMD ? 4 : 0}
                >
                    <Grid item xs={12} md={6}>
                        <FormInput
                            id="nombre"
                            name="nombre"
                            control={control}
                            rules={FORM_SCHEMA.nombre}
                            errors={errors}
                            placeholder="Nombre"
                            fullWidth
                        />
                    </Grid>
                    
                    <Grid item xs={12} md={6}>
                        <FormInput
                            id="apellidos"
                            name="apellidos"
                            control={control}
                            rules={FORM_SCHEMA.apellidos}
                            errors={errors}
                            placeholder="Apellidos"
                            fullWidth
                        />
                    </Grid>

                    <Grid item xs={12} md={6}>
                        <FormInput
                            type="password"
                            id="password"
                            name="password"
                            control={control}
                            rules={FORM_SCHEMA.password}
                            errors={errors}
                            placeholder="Contraseña"
                            fullWidth
                        />
                    </Grid>

                    <Grid item xs={12} md={6}>
                        <FormInput
                            placeholder="Select"
                            id="select"
                            name="select"
                            control={control}
                            rules={FORM_SCHEMA.select}
                            errors={errors}
                            select
                            fullWidth
                        >
                            {[1, 2, 3, 4, 5].map((option) => (
                                <MenuItem key={option} value={option}>
                                    opcion no. {option}
                                </MenuItem>
                            ))}
                        </FormInput>
                    </Grid>
                </FormSearchTogglableWrapper>
                <Grid container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    columnSpacing={2}
                >
                    <Grid item>
                        <Button
                            type={'button'}
                            onClick={(e) => {
                                e.preventDefault();
                                setIsOpenSearch(prev => !prev);
                            }}
                            variant="contained"
                            disableElevation
                            fullWidth
                            startIcon={isOpenSearch ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                            color={isOpenSearch ? 'error' : 'info'}
                        >
                            {isOpenSearch ? 'Đóng' : 'Mở'}
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            type="submit"
                            variant="contained"
                            disableElevation
                            fullWidth
                            startIcon={<SearchIcon />}
                            color='primary'
                        >
                            Tìm kiếm
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            type="button"
                            variant="contained"
                            disableElevation
                            fullWidth
                            startIcon={<SearchOffIcon />}
                            color="error"
                            onClick={onResetSearch}
                        >
                            Xóa lọc
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            type="button"
                            variant="contained"
                            disableElevation
                            fullWidth
                            startIcon={<AddIcon />}
                            color='success'
                        >
                            Thêm mới
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Card>
	);
}

export default FormSearchSomeTable;