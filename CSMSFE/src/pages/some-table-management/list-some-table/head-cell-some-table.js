const headCellSomeTable = [
	{
		id: 'id',
		numeric: false,
		disablePadding: false,
		label: 'Id',
	},
	{
		id: 'name',
		numeric: false,
		disablePadding: false,
		label: 'Nombre',
	},
	{
		id: 'position',
		numeric: false,
		disablePadding: false,
		label: 'Position',
	},
	{
		id: 'email',
		numeric: false,
		disablePadding: false,
		label: 'Email',
	},
	{
		id: 'salary',
		numeric: true,
		disablePadding: false,
		label: 'Salary',
	},
	{
		id: 'options',
		numeric: true,
		disablePadding: false,
		label: 'Opciones',
	},
];

export default headCellSomeTable;