import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';

import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';
import PersonOffOutlinedIcon from '@mui/icons-material/PersonOffOutlined';

import DataTable from '@/components/dataTable';
import headCellSomeTable from './head-cell-some-table';
// import { useEffect } from 'react';

const ListSomeTable = ( props : any) => {

	return (
		<DataTable
            {...props}
            headCells={headCellSomeTable}
            render={(row: any) => (
                <TableRow hover tabIndex={-1} key={row.id}>
                    <TableCell>{row.id}</TableCell>
                    <TableCell align="left">{row.name}</TableCell>
                    <TableCell align="left">{row?.position}</TableCell>
                    <TableCell align="left">{row?.email}</TableCell>
                    <TableCell align="right">${row.salary.toLocaleString()}</TableCell>
                    <TableCell align="right">
                        <Tooltip title="Chỉnh sửa" arrow>
                            <IconButton
                                aria-label="edit"
                                color="warning"
                                size="small"
                                sx={{ fontSize: 2 }}
                                onClick={(e) => {
                                    e.stopPropagation();
                                }}
                            >
                                <ModeEditOutlineOutlinedIcon fontSize="medium" />
                            </IconButton>
                        </Tooltip>

                        <Tooltip title="Deshabilitar Usuario" arrow>
                            <IconButton
                                aria-label="edit"
                                color="error"
                                size="small"
                                sx={{ fontSize: 2 }}
                                onClick={(e) => {
                                    e.stopPropagation();
                                }}
                            >
                                <PersonOffOutlinedIcon fontSize="medium" />
                            </IconButton>
                        </Tooltip>
                    </TableCell>
                </TableRow>
            )}
        />
	);
}

export default ListSomeTable;