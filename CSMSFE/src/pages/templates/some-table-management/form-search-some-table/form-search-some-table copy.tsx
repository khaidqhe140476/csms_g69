import { useForm } from 'react-hook-form';
// MUI
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import FormInput from '@/components/formInput';

// Icon
import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import SearchOffIcon from '@mui/icons-material/SearchOff';

const FORM_SCHEMA = {
	nombre: { required: 'Campo Nombre es requerido' },
	apellidos: { required: 'Campo Apellidos es requerido' },
	textArea: { required: 'Campo Textarea es requerido' },
	select: { required: 'Campo select es requerido' },
	password: {
		required: 'Campo password es requerido',
		minLength: {
			value: 8,
			message: 'Contraseña muy corta',
		},
	},
};

const FormSearchSomeTable = () => {
	const methods = useForm({
		mode: 'onChange',
		defaultValues: {
			nombre: '',
			apellidos: '',
			password: '',
		},
	});
    const { control, handleSubmit, formState: { errors }, } = methods;

	const onSubmit = (data: any) => console.log(data);

	return (
        <Card component="section">
            <form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                <Grid container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    columnSpacing={2}
                >
                    <Grid item xs={12} md={6}>
                        <FormInput
                            id="nombre"
                            name="nombre"
                            control={control}
                            rules={FORM_SCHEMA.nombre}
                            errors={errors}
                            placeholder="Nombre"
                            fullWidth
                        />
                    </Grid>
                    
                    <Grid item xs={12} md={6}>
                        <FormInput
                            id="apellidos"
                            name="apellidos"
                            control={control}
                            rules={FORM_SCHEMA.apellidos}
                            errors={errors}
                            placeholder="Apellidos"
                            fullWidth
                        />
                    </Grid>

                    <Grid item xs={12} md={6}>
                        <FormInput
                            type="password"
                            id="password"
                            name="password"
                            control={control}
                            rules={FORM_SCHEMA.password}
                            errors={errors}
                            placeholder="Contraseña"
                            fullWidth
                        />
                    </Grid>

                    <Grid item xs={12} md={6}>
                        <FormInput
                            placeholder="Select"
                            id="select"
                            name="select"
                            control={control}
                            rules={FORM_SCHEMA.select}
                            errors={errors}
                            select
                            fullWidth
                        >
                            {[1, 2, 3, 4, 5].map((option) => (
                                <MenuItem key={option} value={option}>
                                    opcion no. {option}
                                </MenuItem>
                            ))}
                        </FormInput>
                    </Grid>
                </Grid>
                <Grid container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    columnSpacing={2}
                >
                    <Grid item>
                        <Button
                            type="submit"
                            variant="contained"
                            disableElevation
                            fullWidth
                            startIcon={<SearchIcon />}
                        >
                            Tìm kiếm
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            type="submit"
                            variant="contained"
                            disableElevation
                            fullWidth
                            startIcon={<SearchOffIcon />}
                            color="error"
                        >
                            Xóa lọc
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            type="submit"
                            variant="contained"
                            disableElevation
                            fullWidth
                            startIcon={<AddIcon />}
                            color='secondary'
                        >
                            Thêm mới
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Card>
	);
}

export default FormSearchSomeTable;