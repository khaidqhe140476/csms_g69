import React from 'react';
import { Controller, Control } from 'react-hook-form';
import { TextField, TextFieldProps, Box } from '@mui/material';
import InputAdornment from '@mui/material/InputAdornment';
import CloseIcon from '@mui/icons-material/Close';
import CheckIcon from '@mui/icons-material/Check';
import InputLabel from '@mui/material/InputLabel';

interface IFormInputProps extends Omit<TextFieldProps, 'name' | 'error' | 'helperText'> {
  name: string;
  rules?: Record<string, any>;
  control: Control<any>;
  errors: Record<string, any>;
  dirtyFields?: Record<string, boolean>;
  element?: React.ElementType;
  children?: React.ReactNode;
}

const FormInput: React.FC<IFormInputProps> = ({
  name,
  rules,
  control,
  errors,
  dirtyFields,
  element: InputComponent = TextField,
  children,
  ...otherProps
}) => {
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field }) => (
        <Box sx={{ mb: 2 }}>
          <InputLabel htmlFor={name}>
            Bootstrap
          </InputLabel>
          <InputComponent
            error={!!errors?.[name]}
            helperText={errors?.[name] ? errors[name].message : ' '}
            {...otherProps}
            {...field}
            InputProps={{
              endAdornment: dirtyFields?.[name] && (
                <InputAdornment position="end" sx={{ mr: otherProps?.select ? '16px' : '' }}>
                  {errors?.[name] ? <CloseIcon color="error" /> : <CheckIcon color="success" />}
                </InputAdornment>
              ),
            }}
          >
            {children}
          </InputComponent>
        </Box>
      )}
    />
  );
}

export default FormInput;
