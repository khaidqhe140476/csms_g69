import React, { useState, MouseEvent, ChangeEvent } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import TableContainer, { TableContainerProps } from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableSortLabel from '@mui/material/TableSortLabel';

interface HeadCell {
  id: string;
  numeric: boolean;
  disablePadding: boolean;
  label: string;
}

interface EnhancedTableHeadProps {
  order: 'asc' | 'desc';
  orderBy: string;
  onRequestSort: (event: MouseEvent<unknown>, property: string) => void;
  headCells: HeadCell[];
}

interface EnhancedTableProps {
  rows: any[];
  headCells: HeadCell[];
  render: (row: any, index: number) => React.ReactNode;
  stickyHeader?: boolean;
  tableContainerProps?: TableContainerProps;
  totalItemCount: number;
  handleFilterAction: (
    pageIndex?: number, 
    pageSize?: number, 
    sortExpression?: string, 
    searchParam?: any
  ) => void;
}

// function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
//   if (b[orderBy] < a[orderBy]) {
//     return -1;
//   }
//   if (b[orderBy] > a[orderBy]) {
//     return 1;
//   }
//   return 0;
// }

// function getComparator<Key extends keyof any>(
//   order: 'asc' | 'desc',
//   orderBy: Key
// ): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
//   return order === 'desc'
//     ? (a, b) => descendingComparator(a, b, orderBy)
//     : (a, b) => -descendingComparator(a, b, orderBy);
// }

const EnhancedTableHead: React.FC<EnhancedTableHeadProps> = ({ order, orderBy, onRequestSort, headCells }) => {
  const createSortHandler = (property: string) => (event: MouseEvent<unknown>) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead sx={{ bgcolor: 'background.default' }}>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span style={{ fontSize: '10px' }}>
                  {order === 'desc' ? '•Giảm dần' : '•Tăng dần'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

const EnhancedTable: React.FC<EnhancedTableProps> = (props) => {
  const {
    rows,
    headCells,
    render,
    stickyHeader,
    tableContainerProps,
    totalItemCount,
    handleFilterAction
  } = props;
  const [order, setOrder] = useState<'asc' | 'desc'>('desc');
  const [orderBy, setOrderBy] = useState<string>('');
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);

  const handleRequestSort = (_event: MouseEvent<unknown>, property: string) => {
    const isAsc = orderBy === property && order === 'asc';
    let sort = isAsc ? 'desc' as const : 'asc' as const;
    
    setOrder(sort);
    setOrderBy(property);
    
    handleFilterAction(page + 1, rowsPerPage, `${property} ${sort}`);

  };

  const handleChangePage = (_event: unknown, newPage: number) => {
    setPage(newPage);

    handleFilterAction(newPage, rowsPerPage, `${orderBy} ${order}`);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    const val: number = parseInt(event.target.value, 10);
    setRowsPerPage(val);
    setPage(0);

    handleFilterAction(1, val, `${orderBy} ${order}`);
  };

  return (
    <>
      <TableContainer {...tableContainerProps}>
        <Table
          sx={{ width: '100%' }}
          stickyHeader={stickyHeader}
          aria-labelledby="tableTitle"
          size={'small'}
        >
          <EnhancedTableHead
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            headCells={headCells}
          />
          <TableBody>
            {rows
              // .slice()
              // .sort(getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, i) => render(row, i))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        showFirstButton
        showLastButton
        rowsPerPageOptions={[5, 10, 25, 50]}
        component="div"
        count={totalItemCount}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        labelRowsPerPage="Số bản ghi mỗi trang : "
      />
    </>
  );
};

export default EnhancedTable;
